package edu.westga.cs1302.pricecalculator.view;

import edu.westga.cs1302.pricecalculator.viewmodel.PriceCalculatorViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * PriceCalculatorCodeBehind Class
 * 
 * @author jeremy.trimble
 * @version 11/6/2018
 */
public class PriceCalculatorCodeBehind {

	@FXML
	private Label unitPriceLabel;

	@FXML
	private Label maxDiscountLabel;

	@FXML
	private Label qtyLabel;

	@FXML
	private Button generateButton;

	@FXML
	private TextField unitPriceField;

	@FXML
	private TextField qtyField;

	@FXML
	private TextField maxDiscountField;

	@FXML
	private TextArea priceAnalysisTextArea;

	private PriceCalculatorViewModel calculatorVM;

	/**
	 * Sets the initial value for fxml elements
	 * 
	 * 
	 */
	@FXML
	public void initialize() {
		this.priceAnalysisTextArea.setEditable(false);
		this.calculatorVM = new PriceCalculatorViewModel();
		this.priceAnalysisTextArea.textProperty().bind(this.calculatorVM.getPriceSummary());
		this.unitPriceField.textProperty().bindBidirectional(this.calculatorVM.getUnitPrice(),
				new NumberStringConverter());
		this.qtyField.textProperty().bindBidirectional(this.calculatorVM.getQuantity(), new NumberStringConverter());
		this.maxDiscountField.textProperty().bindBidirectional(this.calculatorVM.getMaxDiscount(),
				new NumberStringConverter());
	}

	/**
	 * Generates a table of the discounts and discounted rates
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void generatePriceAnalysis() {
		this.calculatorVM.generatePriceAnalysis();
	}
}
