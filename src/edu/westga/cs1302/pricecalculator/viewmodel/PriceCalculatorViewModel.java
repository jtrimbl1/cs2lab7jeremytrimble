package edu.westga.cs1302.pricecalculator.viewmodel;

import edu.westga.cs1302.pricecalculator.model.PriceCalculator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * PriceCalculatorViewModel Class
 * 
 * @author jeremy.trimble
 * @version 11/6/2018
 */
public class PriceCalculatorViewModel {
	private PriceCalculator calculator;

	private DoubleProperty unitPrice;
	private IntegerProperty quantity;
	private IntegerProperty maxDiscount;
	private StringProperty priceSummary;

	/**
	 * Creates a new PriceCalculatorViewModel
	 * 
	 * @precondition none
	 * @postcondition new PriceCalculatorViewModel is created.
	 * 
	 */
	public PriceCalculatorViewModel() {
		this.unitPrice = new SimpleDoubleProperty();
		this.quantity = new SimpleIntegerProperty();
		this.maxDiscount = new SimpleIntegerProperty();
		this.priceSummary = new SimpleStringProperty();
		this.calculator = new PriceCalculator();
	}

	/**
	 * Returns the Unit Price
	 * 
	 * @return the unit price
	 */
	public DoubleProperty getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * Returns the Quantity
	 * 
	 * @return the quantity
	 */
	public IntegerProperty getQuantity() {
		return this.quantity;
	}

	/**
	 * Returns the Max Discount
	 * 
	 * @return the max discount
	 */
	public IntegerProperty getMaxDiscount() {
		return this.maxDiscount;
	}

	/**
	 * Returns the Price Analysis Summary
	 * 
	 * @return the price analysis summary
	 */
	public StringProperty getPriceSummary() {
		return this.priceSummary;
	}

	/**
	 * Generates a table of the discounts and discounted rates
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void generatePriceAnalysis() {
		try {
			this.setModelValues();
			String result = String.format("%-20s%-20s%-20s", "Unit Price", "Quantity", "Total Payment")
					+ System.lineSeparator();

			result += String.format("%-23s%-24s%-23s", "$" + this.unitPrice.getValue(), this.quantity.getValue(),
					"$" + (this.calculator.calculateRegularTotal())) + System.lineSeparator() + System.lineSeparator();
			result += String.format("%-23s%-24s%-23s", "Discount %", "Price w\\ discount", "Savings")
					+ System.lineSeparator();
			for (int i = 0; i <= this.maxDiscount.get(); i += 5) {
				result += String.format("%-23s%-40s%-40s", i + "%", "$" + (this.calculator.calculateDiscountedTotal(i)),
						"$" + this.calculator.calculateTotalSavings(i)) + System.lineSeparator();
			}
			this.priceSummary.set(result);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	private void setModelValues() {
		this.calculator.setQuantity(this.quantity.get());
		this.calculator.setUnitPrice(this.unitPrice.get());
	}

}
